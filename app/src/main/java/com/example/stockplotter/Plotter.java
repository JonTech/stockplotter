package com.example.stockplotter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.List;

public class Plotter extends View {


    private final int DATA_POINT_NUM = 500;

    private List<Float> xPosList, yPosList;
    private List<Path> pathList;
    private Path path;
    private Paint paint;

    private ConstraintLayout cl;
    private TextView stockPriceView;




    public Plotter(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.xPosList = new ArrayList<>();
        this.yPosList = new ArrayList<>();
        this.pathList = new ArrayList<>();
        this.paint = new Paint();
        this.paint.setStrokeWidth(20);
        this.paint.setColor(Color.GREEN);


        generateData();
    }

    /***
     * Generates random float data points from 5 to 100 and creates a path to plot
     */
    private void generateData() {

        int min = 5;
        int max = 100;
        double random = 0;

        float xPos = 0;
        float yPos = 0;

        for (int i = 1; i <= this.DATA_POINT_NUM; i++) {
            random = min + Math.random() * (max - min);
            xPos = 50 * i;                                                                          //50 pixels
            yPos = (float)random;

            this.xPosList.add(xPos);
            this.yPosList.add(yPos);

            path = new Path();                                                                      //Create path
            path.moveTo(xPos, yPos);                                                                //Add values to path
            this.pathList.add(path);                                                                //Add path to pathList
        }
    }

    /***
     * Clears the points list
     */
    private void clearData() {
        this.xPosList.clear();
        this.yPosList.clear();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                clearData();
                generateData();
                break;
            case MotionEvent.ACTION_UP:
                invalidate();                                                                       //Refresh canvas
                break;
        }
        return true;                                                                                //Activate event
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint p = new Paint();
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        p.setColor(Color.GREEN);
        p.setStrokeWidth(10);
        p.setStyle(Paint.Style.FILL);
        /***
         * Better use 50 by 50 pixels
         */



        float startX = 0;                                                                           //Start graph at bottom left
        float startY = 0;                                                          //Start at the bottom (max height)

        float nextX = 0;
        float nextY = 0;

        float viewWidth = getWidth();
        float viewHeight = getHeight();
        for (int i = 0; i < this.DATA_POINT_NUM; i++) {

            nextX = this.xPosList.get(i);
            nextY = this.yPosList.get(i);
            canvas.drawLine(viewWidth * startX / 1000, viewHeight - (viewHeight * startY / 100),
                    viewWidth * nextX / 1000, viewHeight - (viewHeight * nextY / 100), p);                                       //Draw segment

            startX = nextX;                                                                         //Save previous X point
            startY = nextY;                                                                         //Save previous Y point
        }


        //TODO: Find a better way to manage this
        cl = (ConstraintLayout) ((ViewGroup)this.getParent());                                      //get parent Layout
        this.stockPriceView = cl.findViewById(R.id.stockPriceText);                                 //access the sibling
        if (this.stockPriceView != null) {
            this.stockPriceView.setText((nextY)+"");                                           //Write number
        }
    }
}
